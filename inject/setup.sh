#!/bin/bash -x

source /host/settings.sh

main() {
    fix_redis
    create_db
    create_uwsgi_config
    create_uwsgi_service
    setup_apache2
    create_celery_config

    if [[ ! $(ls -A /opt/indico) ]]; then
        cp -a /opt/indico1/* /opt/indico/
        cp -a /opt/indico1/.[^.]* /opt/indico/
        chown indico:www-data -R /opt/indico/

        install_pyenv
        install_indico
        setup_indico
    fi
    
    enable_services
}

fix_redis() {
    mkdir -p /lib/systemd/system/redis-server.service.d
    cat <<EOF > /lib/systemd/system/redis-server.service.d/override.conf
[Service]
PrivateUsers=false
ProtectHostname=false
EOF
    systemctl daemon-reload
    systemctl restart redis
}

create_db() {
    if [[ $(ls -A /var/lib/postgresql) ]]; then
        chown postgres: -R /var/lib/postgresql/
        systemctl restart postgresql.service
    else
        cp -a /var/lib/postgresql1/* /var/lib/postgresql/
        chown postgres: -R /var/lib/postgresql/
        systemctl restart postgresql.service
    
        su - postgres -c 'createuser indico'
        su - postgres -c 'createdb -O indico indico'
        su - postgres -c 'psql indico -c "CREATE EXTENSION unaccent; CREATE EXTENSION pg_trgm;"'
    fi
}

create_uwsgi_config(){
    cat > /etc/uwsgi-indico.ini <<'EOF'
[uwsgi]
uid = indico
gid = www-data
umask = 027

processes = 4
enable-threads = true
socket = 127.0.0.1:8008
stats = /opt/indico/web/uwsgi-stats.sock
protocol = uwsgi

master = true
auto-procname = true
procname-prefix-spaced = indico
disable-logging = true

single-interpreter = true

touch-reload = /opt/indico/web/indico.wsgi
wsgi-file = /opt/indico/web/indico.wsgi
virtualenv = /opt/indico/.venv

vacuum = true
buffer-size = 20480
memory-report = true
max-requests = 2500
harakiri = 900
harakiri-verbose = true
reload-on-rss = 2048
evil-reload-on-rss = 8192
EOF
}

create_uwsgi_service() {
    cat > /etc/systemd/system/indico-uwsgi.service <<'EOF'
[Unit]
Description=Indico uWSGI
After=network.target

[Service]
ExecStart=/opt/indico/.venv/bin/uwsgi --ini /etc/uwsgi-indico.ini
ExecReload=/bin/kill -HUP $MAINPID
Restart=always
SyslogIdentifier=indico-uwsgi
User=indico
Group=www-data
UMask=0027
Type=notify
NotifyAccess=all
KillMode=mixed
KillSignal=SIGQUIT
TimeoutStopSec=300

[Install]
WantedBy=multi-user.target
EOF
}

setup_apache2() {
    cat > /etc/apache2/sites-available/indico-sslredir.conf <<'EOF'
<VirtualHost *:80>
    ServerName YOURHOSTNAME
    RewriteEngine On
    RewriteRule ^(.*)$ https://%{HTTP_HOST}$1 [R=301,L]
</VirtualHost>
EOF
    sed -i /etc/apache2/sites-available/indico-sslredir.conf \
        -e "s/YOURHOSTNAME/$DOMAIN/"

    cat > /etc/apache2/sites-available/indico.conf <<'EOF'
<VirtualHost *:443>
    ServerName YOURHOSTNAME
    DocumentRoot "/var/empty/apache"
    Protocols h2 http/1.1

    SSLEngine             on
    SSLCertificateFile    /etc/ssl/indico/indico.crt
    SSLCertificateKeyFile /etc/ssl/indico/indico.key

    SSLProtocol           all -SSLv3 -TLSv1 -TLSv1.1
    SSLCipherSuite        ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
    SSLHonorCipherOrder   off
    SSLSessionTickets     off

    XSendFile on
    XSendFilePath /opt/indico
    CustomLog /opt/indico/log/apache/access.log combined
    ErrorLog /opt/indico/log/apache/error.log
    LogLevel error
    ServerSignature Off

    <If "%{HTTP_HOST} != 'YOURHOSTNAME'">
        Redirect 301 / https://YOURHOSTNAME/
    </If>

    AliasMatch "^/(images|fonts)(.*)/(.+?)(__v[0-9a-f]+)?\.([^.]+)$" "/opt/indico/web/static/$1$2/$3.$5"
    AliasMatch "^/(css|dist|images|fonts)/(.*)$" "/opt/indico/web/static/$1/$2"
    Alias /robots.txt /opt/indico/web/static/robots.txt

    SetEnv UWSGI_SCHEME https
    ProxyPass / uwsgi://127.0.0.1:8008/

    <Directory /opt/indico>
        AllowOverride None
        Require all granted
    </Directory>
</VirtualHost>
EOF
    sed -i /etc/apache2/sites-available/indico.conf \
        -e "s/YOURHOSTNAME/$DOMAIN/"

    # enable modules and sites
    a2enmod proxy_uwsgi rewrite ssl xsendfile
    a2dissite 000-default
    a2ensite indico indico-sslredir

    # create a self-signed TLS certificate
    mkdir /etc/ssl/indico
    chown root:root /etc/ssl/indico/
    chmod 700 /etc/ssl/indico
    openssl req -x509 -nodes -newkey rsa:4096 \
            -subj /CN=$DOMAIN \
            -keyout /etc/ssl/indico/indico.key \
            -out /etc/ssl/indico/indico.crt
}

create_celery_config() {
    # add a systemd unit file for celery
    cat > /etc/systemd/system/indico-celery.service <<'EOF'
[Unit]
Description=Indico Celery
After=network.target

[Service]
ExecStart=/opt/indico/.venv/bin/indico celery worker -B
Restart=always
SyslogIdentifier=indico-celery
User=indico
Group=www-data
UMask=0027
Type=simple
KillMode=mixed
TimeoutStopSec=300

[Install]
WantedBy=multi-user.target
EOF
}


install_pyenv() {
    # install pyenv
    cd /opt/indico
    curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer \
         -o pyenv-installer
    su - indico -c 'bash pyenv-installer'
    rm pyenv-installer

    cat >> /opt/indico/.profile <<'EOF'
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init --path)"
eval "$(pyenv init -)"
EOF
}

install_indico() {
    cat > /opt/indico/install.sh <<'EOF'
#!/bin/bash
export TMPDIR=/opt/indico/.pyenv/tmp

pyver=$(pyenv install --list | egrep '^\s*3\.9\.' | tail -1)
pyenv install $pyver
pyenv global $pyver

python -m venv --upgrade-deps --prompt indico ~/.venv
source ~/.venv/bin/activate
echo 'source ~/.venv/bin/activate' >> ~/.profile

pip install wheel
pip install uwsgi
pip install indico
pip install indico-plugins
EOF
    chmod +x /opt/indico/install.sh
    su - indico -c ./install.sh
    rm /opt/indico/install.sh
}

setup_indico() {
    ### this does the job of 'indico setup wizard' etc.
    
    # create directories
    mkdir -p /opt/indico/{archive,cache,log/apache,etc,tmp,web,custom}

    # copy some config files
    local lib_dir='/opt/indico/.venv/lib/python3.9/site-packages/indico'
    cp $lib_dir/logging.yaml.sample \
       /opt/indico/etc/logging.yaml
    cp $lib_dir/web/indico.wsgi \
       /opt/indico/web/indico.wsgi
    ln -s $lib_dir/web/static \
       /opt/indico/web/static

    # create etc/indico.conf
    local secret_key=$(echo "python -c 'import os; print(repr(os.urandom(32)))'" \
                           | su - indico -c 'bash -s' 2>/dev/null)
    cat > /opt/indico/etc/indico.conf <<EOF
# General settings
SQLALCHEMY_DATABASE_URI = 'postgresql:///indico'
SECRET_KEY = $secret_key
BASE_URL = 'https://$DOMAIN'
CELERY_BROKER = 'redis://127.0.0.1:6379/0'
REDIS_CACHE_URL = 'redis://127.0.0.1:6379/1'
# DEFAULT_TIMEZONE = 'UTC'
# DEFAULT_LOCALE = 'en_GB'
ENABLE_ROOMBOOKING = False
CACHE_DIR = '/opt/indico/cache'
TEMP_DIR = '/opt/indico/tmp'
LOG_DIR = '/opt/indico/log'
CUSTOMIZATION_DIR = '/opt/indico/custom'
STORAGE_BACKENDS = {'default': 'fs:/opt/indico/archive'}
ATTACHMENT_STORAGE = 'default'

# Email settings
# SMTP_SERVER = ('smtp.example.org', 25)
# SMTP_USE_TLS = False
# SMTP_LOGIN = ''
# SMTP_PASSWORD = ''
# SUPPORT_EMAIL = 'user@gmail.com'
# PUBLIC_SUPPORT_EMAIL = 'user@gmail.com'
# NO_REPLY_EMAIL = 'noreply@gmail.com'

STATIC_FILE_METHOD = 'xsendfile'
XELATEX_PATH = '/opt/texlive/bin/x86_64-linux/xelatex'
USE_PROXY = True
EOF
    # create a symlink to the config file
    ln -s /opt/indico/etc/indico.conf \
       /opt/indico/.indico.conf

    # fix owneship and permissions
    chown indico:www-data -R /opt/indico/
    chmod go-rwx /opt/indico/* /opt/indico/.[^.]*
    chmod 710 /opt/indico/{,archive,cache,log,tmp}
    chmod 750 /opt/indico/{web,.venv,custom}
    chmod g+w /opt/indico/log/apache
    
    # setup db
    su - indico -c 'bash -c "indico db prepare"'
    su - indico -c 'bash -c "indico db --all-plugins upgrade"'
}

enable_services() {
    systemctl daemon-reload
    systemctl restart \
              apache2.service \
              indico-celery.service \
              indico-uwsgi.service
    systemctl enable \
              apache2.service \
              postgresql.service \
              redis-server.service \
              indico-celery.service \
              indico-uwsgi.service
}

# start main
main "$@"
