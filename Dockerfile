include(bookworm)

RUN <<EOF
  apt update
  apt upgrade --yes
  apt install --yes \
      lsb-release \
      wget \
      curl \
      gnupg \
      git

  gpg_key=/usr/share/keyrings/pgdg.gpg
  wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc \
      | gpg --dearmour -o $gpg_key
  repo_url=http://apt.postgresql.org/pub/repos/apt/
  echo "deb [arch=amd64 signed-by=$gpg_key] $repo_url $(lsb_release -cs)-pgdg main" \
      > /etc/apt/sources.list.d/pgdg.list
  apt update

  DEBIAN_FRONTEND=noninteractive \
      apt install --yes --install-recommends \
          postgresql-13 \
          libpq-dev \
          apache2 \
          libapache2-mod-proxy-uwsgi \
          libapache2-mod-xsendfile \
          libxslt1-dev \
          libxml2-dev \
          libffi-dev \
          libpcre3-dev \
          libyaml-dev \
          libssl-dev \
          libbz2-dev \
          libreadline-dev \
          libsqlite3-dev \
          libncurses5-dev \
          libncursesw5-dev \
          xz-utils \
          liblzma-dev \
          uuid-dev \
          build-essential \
          redis-server \
          libjpeg62-turbo-dev \
          zlib1g-dev
EOF

### install latex
COPY misc/texlive.profile /tmp/
RUN <<EOF
  apt install --yes libfontconfig1 ghostscript
  cd /tmp
  wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
  tar xvzf install-tl-unx.tar.gz
  cd install-tl-*
  ./install-tl --profile /tmp/texlive.profile
EOF

RUN <<EOF
  # create a user that will be used to run indico
  useradd -rm -g www-data -d /opt/indico -s /bin/bash indico

  # backup directories that will be mounted
  cp -a /opt/indico /opt/indico1
  cp -a /var/lib/postgresql /var/lib/postgresql1

EOF
