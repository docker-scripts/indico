cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    mkdir -p indico postgresql
    orig_cmd_create \
        --mount type=bind,src=$(pwd)/indico,dst=/opt/indico \
        --mount type=bind,src=$(pwd)/postgresql,dst=/var/lib/postgresql \
        "$@"    # accept additional options
}
