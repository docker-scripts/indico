cmd_backup_help() {
    cat <<_EOF
    backup
        Make a backup.

_EOF
}

cmd_backup() {
    # create the backup dir
    local backup="backup-$(date +%Y%m%d)"
    rm -rf $backup
    rm -f $backup.tgz
    mkdir $backup

    # dump the content of the database
    ds exec su - indico -c 'pg_dump -c indico' > $backup/indico.sql

    # backup the archive
    cp -a indico/archive/ $backup/

    # backup the config file
    cp indico/etc/indico.conf $backup/

    # backup customizations
    cp -a indico/custom/ $backup/
    
    # backup settings
    cp settings.sh $backup/

    # make the backup archive
    tar --create --gzip --preserve-permissions --file=$backup.tgz $backup/
    rm -rf $backup/
}
