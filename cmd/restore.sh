cmd_restore_help() {
    cat <<_EOF
    restore <backup-file.tgz>
        Restore from the given backup file.

_EOF
}

cmd_restore() {
    local file=$1
    [[ ! -f $file ]] && fail "Usage:\n$(cmd_restore_help)"
    local backup=${file%%.tgz}
    backup=$(basename $backup)

    # extract the backup archive
    tar --extract --gunzip --preserve-permissions --file=$file

    # restore the content of the database
    ds exec su - indico -c "psql -q -f /host/$backup/indico.sql indico"

    # restore the archive
    rm -rf indico/archive
    cp -a $backup/archive/ indico/

    # restore the config file
    cp $backup/indico.conf indico/etc/

    # restore customizations
    rm -rf indico/custom
    cp -a $backup/custom/ indico/

    # fix ownership
    ds exec chown indico:www-data -R /opt/indico/
    
    # restore settings
    #cp $backup/settings.sh .

    # clean up
    rm -rf $backup
}
