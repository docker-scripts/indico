# indico in a container

https://getindico.io/

## Installation

  - First install `ds` and `revproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull indico`

  - Create a directory for the container: `ds init indico @events.example.org`

  - Fix the settings: `cd /var/ds/events.example.org/ ; vim settings.sh`

  - Make the container: `ds make`


## Configuration

Edit `indico/etc/indico.conf` and then `ds restart`.


## Update

```bash
docker pull dockerscripts/indico
ds make
```
